package com.example.cswexam.api;

import com.example.cswexam.entity.Product;
import com.example.cswexam.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
@RequestMapping("/api/v1/products")
public class ProductAPI {
    final private ProductService productService;

    public ProductAPI(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<Product>> findAll(){
        return ResponseEntity.ok(productService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Product product){
        return ResponseEntity.ok(productService.save(product));
    }
    @GetMapping("/{id}")
    public  ResponseEntity<Product> findById(@PathVariable Integer id){
        Optional<Product> product = productService.findById(id);
        if (!product.isPresent()){
            log.error("id not existed");
        }
        return ResponseEntity.ok(product.get());
    }
    @PutMapping
    public ResponseEntity<Product> update(@PathVariable Integer id,
                                          @Valid @RequestBody Product product ){
//        Optional<Product> product1 = productService.findById(id);
        if (!productService.findById(id).isPresent()){
            log.error("id not existed");
        }
        product.setId(id);

        return ResponseEntity.ok(productService.save(product));

    }

    @DeleteMapping("/{id}")
        public ResponseEntity delete(@PathVariable Integer id){
            if (!productService.findById(id).isPresent()){
                log.error("id not existed");
            }
            productService.deletedById(id);
            return ResponseEntity.ok().build();
        }

}
