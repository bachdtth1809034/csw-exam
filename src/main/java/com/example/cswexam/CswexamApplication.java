package com.example.cswexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CswexamApplication {

    public static void main(String[] args) {
        SpringApplication.run(CswexamApplication.class, args);
    }

}
